require "test_helper"

class ProjectsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @project = projects(:one)
    sign_in users(:one)
  end

  test "should get index" do
    get projects_url
    assert_response :success
  end

  test "should be logged in" do
    sign_out users(:one)
    get projects_url
    assert_redirected_to new_user_session_path
  end

  test "should get new" do
    get new_project_url
    assert_response :success
  end

  test "should create project owned by the current user" do
    assert_difference("Project.count") do
      post projects_url, params: { project: { status: @project.status, title: @project.title } }
    end

    assert_redirected_to project_url(Project.last)
  end

  test "should show project" do
    get project_url(@project)
    assert_response :success
  end

  test "should get edit" do
    get edit_project_url(@project)
    assert_response :success
  end

  test "should update project" do
    patch project_url(@project), params: { project: { status: @project.status, title: @project.title } }
    assert_redirected_to project_url(@project)
  end

  test "adds event when updating project status" do
    assert_difference "Event.count" do
      patch project_url(@project), params: { project: { status: "In progress" } }
    end
  end

  test "should destroy project" do
    assert_difference("Project.count", -1) do
      delete project_url(@project)
    end

    assert_redirected_to projects_url
  end

  test "should create comment owned by the current user" do
    assert_difference("Event.count") do
      assert_difference("Comment.count") do
        post comment_project_url(@project, params: { comment: { content: "Hello" } })
      end
    end
    assert_equal users(:one), Comment.last.user
    assert_equal "#{users(:one).email} says Hello", Event.last.details

    assert_redirected_to project_url(@project)
  end
end
