require "test_helper"

class ProjectTest < ActiveSupport::TestCase
  test "status defaults to not started" do
    assert_equal('not_started', Project.new.status)
  end
end
