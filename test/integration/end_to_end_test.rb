require "test_helper"

class EndToEndTest < ActionDispatch::IntegrationTest
  test "the truth" do
    get projects_path
    assert_redirected_to new_user_session_path

    follow_redirect!
    post new_user_session_path, params: { user: { email: users(:one).email, password: "password" } }
    assert_redirected_to projects_path

    follow_redirect!
    assert_select "a", text: "New project"
    post projects_path, params: { project: { name: "My Project" } }
    project = Project.last
    assert_redirected_to project_path(project)

    follow_redirect!
    assert_select "a", text: "Edit this project"
    get edit_project_path(project)
    assert_select "input", value: "Update project"
    patch project_path(project, params: { project: { status: "New status" } })
    follow_redirect!
    assert_select "h2", text: "History"
    assert_select "#event_#{Event.last.id} > p:nth-child(2)", text: /Status changed from Not started to New status/

    post comment_project_path(project, params: { comment: { content: "hello" } })
    follow_redirect!
    assert_select "#event_#{Event.last.id} > p:nth-child(2)", text: /#{users(:one).email} says #{Comment.last.content}/
    # log out
    # login as another user
    # visit project
    # cannot change status (auth)
    # submit comment
    # see both users comment
  end
end
