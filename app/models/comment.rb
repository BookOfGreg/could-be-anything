class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :project

  around_create :track_comment

  def track_comment
    transaction do
      Event.create(project: project, user: user, details: "#{user.email} says #{content}")
      yield
    end
  end
end
