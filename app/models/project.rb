class Project < ApplicationRecord
  belongs_to :user
  has_many :events, dependent: :nullify
  has_many :comments, dependent: :destroy

  def update_tracked(params, user)
    assign_attributes(params)
    if status_changed?
      self.events.build(user: user, details: "Status changed from #{status_was.humanize} to #{status.humanize}")
    end
    update(params)
  end
end
