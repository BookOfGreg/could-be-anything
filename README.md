# Project Manager

Basic ERD for what I wanted to build based on your description.
![draw.io diagram at root of project](./projects.drawio.png)

- User can change status, tracked as events.
- User can leave comments, also tracked as events to make presentation simple.
- Events created in transaction for safety (spent too long on "unsafe" databses lately)

## Deployment

Hosted on Fly.io at https://homey-project.fly.dev/
User accounts found in the user.yml file for convenience, prod has been seeded with the text fixtures, also for convenience.

## Git host

Hosted on GitLab because I would prefer people not to see it on my GitHub attached to the ReactJS or RSwag membership.
https://gitlab.com/BookOfGreg/could-be-anything

## Questions and hypothetical answers:

- Who is the project history for?
  - Project managers who want to see everything that changed on a project in a stream. Think JIRA.
- Is anyone allowed to change a project status?
  - Only the creator and their authorized users (Not implemented authz but easy enough once ownership is established via user-ids)
- What happens when a project is completed and done?
  - It gets deleted completely
- What about comments?
  - Delete those too
- What about the project history?
  - No, that's our audit log, we need that.
- So shall I soft-delete the project?
  - Just delete it, we can figure out what happened from the events.
