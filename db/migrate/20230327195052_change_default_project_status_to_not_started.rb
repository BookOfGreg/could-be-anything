class ChangeDefaultProjectStatusToNotStarted < ActiveRecord::Migration[7.0]
  def change
    change_column_default :projects, :status, 'not_started'
  end
end
