class CreateEvents < ActiveRecord::Migration[7.0]
  def change
    create_table :events do |t|
      t.references :user, null: false, foreign_key: true
      t.string :details
      t.references :project, null: false, foreign_key: true, null: true

      t.timestamps
    end
  end
end
